package main

import (
	"encoding/json"
	"net/http"
	"time"

	"weight/db"
)

type saveWeightRequest struct {
	After  bool `json:"after"`
	Weight int  `json:"weight"`
}

type saveWeightResponse struct {
	Difference        int  `json:"difference"`
	IncorrectSequence bool `json:"IncorrectSequence"`
}

func saveWeight(w http.ResponseWriter, req *http.Request) {
	r := &saveWeightRequest{}
	err := json.NewDecoder(req.Body).Decode(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	last, err := babyManager.GetLastWeight()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	err = babyManager.SaveWeight(db.WeightInfo{
		Weight:       r.Weight,
		AfterNursing: r.After,
		RecordedAt:   time.Now(),
	})

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp := saveWeightResponse{
		Difference:        r.Weight - last.Weight,
		IncorrectSequence: r.After == last.AfterNursing,
	}
	data, _ := json.Marshal(resp)
	_, _ = w.Write(data)
}
