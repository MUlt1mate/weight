package db

import (
	"database/sql"
	"time"
)

type WeightInfo struct {
	Weight       int
	AfterNursing bool
	RecordedAt   time.Time
}

type WeightRepository interface {
	SaveWeight(w WeightInfo) (err error)
	GetLastWeight() (w WeightInfo, err error)
	CreateSchema() (err error)
}

type weightRepository struct {
	db *sql.DB
}

func NewWeightRepository(db *sql.DB) WeightRepository {
	return &weightRepository{db: db}
}

func (m *weightRepository) SaveWeight(w WeightInfo) (err error) {
	query := `
INSERT INTO records (weight, is_after_nursing)
VALUES ($1, $2)`
	_, err = m.db.Exec(query, w.Weight, w.AfterNursing)
	return err
}

func (m *weightRepository) GetLastWeight() (w WeightInfo, err error) {
	query := `
SELECT weight, is_after_nursing, recorded_at
FROM records
ORDER BY id DESC
LIMIT 1`
	err = m.db.QueryRow(query).Scan(
		&w.Weight,
		&w.AfterNursing,
		&w.RecordedAt,
	)
	return w, err
}

func (m *weightRepository) CreateSchema() (err error) {
	query := `
CREATE TABLE IF NOT EXISTS records 
(
    id               INTEGER,
    weight           INTEGER,
    is_after_nursing INTEGER,
    recorded_at      DATETIME DEFAULT CURRENT_TIMESTAMP,
    PRIMARY KEY ("id" AUTOINCREMENT)
)`
	_, err = m.db.Exec(query)
	return err
}
