package main

import (
	"database/sql"
	"errors"
	"net/http"
	"os"

	"weight/baby"
	"weight/db"

	_ "github.com/mattn/go-sqlite3"
)

const dbPath = "./records.db"

var babyManager baby.Manager

func main() {
	var migrationRequired bool
	if _, err := os.Stat(dbPath); errors.Is(err, os.ErrNotExist) {
		if _, err = os.Create(dbPath); err != nil {
			panic(err)
		}
		migrationRequired = true
	}

	fileDb, err := sql.Open("sqlite3", dbPath)
	if err != nil {
		panic(err)
	}
	defer fileDb.Close()
	repo := db.NewWeightRepository(fileDb)
	if migrationRequired {
		err = repo.CreateSchema()
		if err != nil {
			panic(err)
		}
	}
	babyManager = baby.GetManager(repo)

	http.HandleFunc("/weight", saveWeight)
	_ = http.ListenAndServe(":8091", nil)
}
