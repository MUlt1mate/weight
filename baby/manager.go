package baby

import (
	"weight/db"
)

type Manager interface {
	SaveWeight(w db.WeightInfo) (err error)
	GetLastWeight() (w db.WeightInfo, err error)
}

type manager struct {
	repo db.WeightRepository
}

func GetManager(repo db.WeightRepository) Manager {
	return &manager{repo: repo}
}

func (m *manager) SaveWeight(w db.WeightInfo) (err error) {
	return m.repo.SaveWeight(w)
}

func (m *manager) GetLastWeight() (w db.WeightInfo, err error) {
	return m.repo.GetLastWeight()
}
